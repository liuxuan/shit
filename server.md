## 数据库设计：
```
table m3u8(
id
uri
m3u8_attributes (BANDWIDTH, CODECS, RESOLUTION, FRAME-RATE, AUDIO, VIDEO, ...)
)

table ts(
id
m3u8id
uri
sequence
duration
title
filesize
crc32
)

table peer(
peerid
peer_detail(load, addr, ...)
)

relation m3u8_peer(
m3u8id
ts_range(sequence_begin, sequence_end)
peerid
)

relation peer_m3u8(
peerid
m3u8id
ts_range(sequence_begin, sequence_end)
)
```
> ts_range前期暂不考虑使用  

目前只需要通过主键查询，可使用KV数据库。但后期有可能需要在sequence和ts_range等字段上建立索引，考虑到后期的可扩展性以及数据持久化需求，目前首选mongodb或mysql。

## 流程：
### 客户端查索引流程
`m3u8_url -> m3u8_id -> ts_id list -> ts_detail list`  
对于点播，可以选择从源地址或索引服务器获取m3u8；对于直播，应优先从源服务器获取m3u8文件。 **建议所有情况均从源地址获取m3u8，这样索引服务器可以不用过多关心m3u8本身，而是仅仅对ts查源，可以简化设计。** 如果需要提供查索引服务，则需保证ts列表顺序正确且完整，当同一个m3u8先后收到两次ts列表上报时，服务器需使用sequence机制来保证ts顺序正确，而sequence在少数情况下可能会不连续或回绕，这会增加复杂性。

### 客户端查peer资源流程
`m3u8_url -> m3u8_id -> peer list -> peer_detail list`  
`------(&ts range)-----`（后续应考虑使用ts_range提高查询准确度）  

## 客户端身份校验
暂不考虑

## 服务器设计
设计目标：较高的伸缩性，自动分区。服务器自身以及数据暂不考虑通过冗余增加可用性，某一点服务器宕机会导致与之相关的m3u8不可用，直至服务恢复为止。

### m3u8索引服务器设计：
分区粒度为单个m3u8（一个m3u8的ts列表和peer资源列表都在同一个分区内）  
分区方法：`hash(m3u8uri)%100000 --> shard_id`（可考虑使用一致性哈希）  
m3u8id计算方法：`m3u8_id = shard_id<<32 | db_id`

#### m3u8_meta_server：  

| shard_id start | shard id end | node server addr | node server load | node server statistics |
|---| ---| ---| ---| ---|
|0              | shard_id1    |node_1 host:port|-|-|
|shard_id1+1    | shard_id2    |node_2 host:port|-|-|
|...            |...           |...             |-|-|
|...            |100000        |node_n host:port|-|-|

*  node server:  
一般情况下，这只包含一个服务和对应数据库，用来处理一定数量的m3u8。  
对于热点m3u8，这是一个集群，  
内部结构仍然是meta+node+route， 内部按照peer区域或peerid来分片。  
目前不考虑对服务或数据进行主从备份。  

* load&statistics:  
由node server周期性上报。其中statistics是个json字符串，应包含每个请求每秒中的处理数量等信息，meta只作存储，不关心其具体内容。考虑到每个node server的处理能力可能不对等，后续可在meta给每个node server预分配个处理能力，node server根据这个值来领取请求。

#### m3u8_node_server:
可先实现以便快速联调  
* table m3u8
* table ts
* relation m3u8_peer 用来查peer资源

#### m3u8_route_server:
缓存m3u8_meta_server的分片信息，将客户端的请求转发到正确的m3u8_server  
这个服务是可选的，如果通过客户端缓存分片信息，则不需要，参见*路由方案二*

#### copy&move
当增加节点时，自动将一部分数据迁移到新节点上，可稍晚实现


### peer服务器设计：

#### peer_meta_server：
参见m3u8_meta_server  

#### peer_server:
* table peer  
* relation peer_m3u8 (可选，一般不需要查一个peer的所有资源)

#### peer_route_server:
参见m3u8_route_server

#### copy&move
当增加节点时，自动将一部分数据迁移到新节点上，可稍晚实现

### 其他
#### 1，路由方案二  
通信协议由服务端统一实现，并提供C语言库给客户端使用，该库可实现自动负载均衡
#### 2，内网（私有IPv4）m3u8过滤
在客户端进行
#### 3，投票
多个peer上报的m3u8信息由冲突，暂无方案



## 协议：
暂定使用protobuf？
### 上报：
目前不分全量和增量（客户端通常不会一次性下载所有ts），每次上报的ts列表均作为一个range。  
通常每个分片时长几秒到几十秒之间，大小则在几百K到几M之间。
```
ts_item
{
ts_url string
filesize int
crc32 int
sequence int
duration float
title string
...
}

m3u8_report_req
{
peerid string
m3u8url string
ts_list array<ts_item>
}

m3u8_report_resp
{
result int
}
```

### 查m3u8索引
延后实现
```
query_m3u8_req
{
m3u8_url string
}

query_m3u8_resp
{
ts_list array<ts_item>
}
```

### 查peer资源
```
query_resource_req
{
m3u8_url string
ts_range ...
}

peer_item
{
peerid string
addr string
nattype int8
ts_range ...
}
query_resource_resp
{
peer_list array<peer_item>
}
```
